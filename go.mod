module bitbucket.org/atlassian/go-asap

require (
	github.com/SermoDigital/jose v0.9.2-0.20161205224733-f6df55f235c2
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.2.2
	github.com/vincent-petithory/dataurl v0.0.0-20160330182126-9a301d65acbb
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
